//
//  LCExtensions.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 15/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import UIKit

extension UISearchBar {
    
    /// Align Placeholder Label to Left position
    func alignPlaceholderLeft () {
        let noOffset = UIOffset(horizontal: 0, vertical: 0)
        setPositionAdjustment(noOffset, for: .search)
        showsCancelButton = true
    }
    
    /// Align Placeholder Label to Center position
    func alignPlaceholderCenter () {
        let textFieldInsideUISearchBarLabel = value(forKey: "placeholderLabel") as? UILabel
        let offset = UIOffsetMake((textFieldInsideUISearchBarLabel?.bounds.size.width)!, 0.0)
        
        setPositionAdjustment(offset, for: .search)
        showsCancelButton = false
    }
}
