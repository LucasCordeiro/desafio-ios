//
//  ConstantsCenter.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 13/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import Foundation
import UIKit


enum MovieRequestState : Int {
    case error
    case loading
    case noSearchResult
    case sucess
}
struct MovieAPIKey {
    static let apiKey = "01f4eb0c0ef4ef4047cee163a5220646"
}
/// All colors to  follow a pattern
struct ColorConst {
    static var mainYellowColor = #colorLiteral(red: 0.968627451, green: 0.8078431373, blue: 0.3568627451, alpha: 1)
    static var mainBlueColor = #colorLiteral(red: 0.1764705882, green: 0.1882352941, blue: 0.2784313725, alpha: 1)
    static var mainOrangeColor = #colorLiteral(red: 0.8509803922, green: 0.5921568627, blue: 0.1176470588, alpha: 1)
    static var mainWhiteColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
}
/// Server Infos/links/keys
struct ServerInfo {
    
    ///Var to define if is testing or in production
    static let isProductionSever : Bool = true
    
    /// Sever URLs
    struct SeverUrl {
        
        /// URL to access production/testing api
        static let mainURL =  ServerInfo.isProductionSever ? "https://api.themoviedb.org/3/" : "https://api.themoviedb.org/3/"
        
        static let imageURL = "http://image.tmdb.org/t/p/w342/"
    }
    
    /// Key value to parameters
    struct APIParameterKey {
        
        /// password key
        static let password = "password"
        /// email key
        static let email = "email"
        
        /// userID key
        static let userID = "userID"
    }
}

/// Http header informations
///
/// - authentication: authentication value - "Authorization"
/// - contentType: content-tupe value - "Content-Type"
/// - acceptType: accept type value - "Accept"
/// - acceptEncoding: accept encoding value - "Accept-Encoding"
enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

/// Content type values
///
/// - json: "application/json"
enum ContentType: String {
    case json = "application/json"
}
