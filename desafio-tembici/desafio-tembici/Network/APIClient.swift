//
//  APIClient.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 13/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import Foundation

/// Class that willl provide services to view models
class APIClient {
    
    /// Perform request with information
    ///
    /// - Parameters:
    ///   - route: route/path
    ///   - decoder: decoder (JSONDecoder as default)
    ///   - completion: completion called after finishing request
    /// - Returns: DataRequest
    @discardableResult
    private static func performRequest<T:Decodable>(route:APIRouter, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T>)->Void) -> DataRequest {
        return SessionManager.default.request(route)
            .responseJSONDecodable (decoder: decoder){ (response: DataResponse<T>) in
                completion(response.result)
        }
    }
    
    /// List Movies from page
    ///
    /// - Parameters:
    ///   - page: page to get movies
    ///   - completion: returns request result
    /// - Returns: data request of current request
    static func listMovies(_ page:Int, completion:@escaping (Result<LCMovieResult>)->Void) -> DataRequest {
        let jsonDecoder = JSONDecoder()
        let route = APIRouter.listMovie(page: page)
        return performRequest(route: route, decoder: jsonDecoder, completion: completion)
    }
}
