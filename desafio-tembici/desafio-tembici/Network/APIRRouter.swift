//
//  APIRRouter.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 13/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import Foundation

/// API that will provide requests informations
enum APIRouter: URLRequestConvertible {
    
    // MARK: - Comunication
    case listMovie(page: Int)
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .listMovie:
            return .get
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .listMovie(let page):
            return "movie/popular?api_key=\(MovieAPIKey.apiKey)&page=\(page > 0 ? page : 1)"
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .listMovie:
            return nil
        }
    }
    
    // MARK: - Parameters
    private var queryItems: Parameters? {
        switch self {
        case .listMovie:
            return nil
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        var url: URL
        
        switch self {
        case .listMovie:
            url = try "\(ServerInfo.SeverUrl.mainURL)\(path)".asURL()
        }
        var urlRequest = URLRequest(url: url)

        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}
