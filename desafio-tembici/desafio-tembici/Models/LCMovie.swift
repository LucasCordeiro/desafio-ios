//
//  Movie.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 15/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import Foundation

struct LCMovie : Codable {
    
    /// Title Movie
    var title : String?
    
    /// Proprierty to says whether movie is favorite
    var isFavorite : Bool? = false
    
    /// Unique ID
    let id : Int? = -1
    
    
    let vote_count : Int?
    let video : Bool?
    let vote_average : Double?
    let popularity : Double?
    let poster_path : String?
    let original_language : String?
    let original_title : String?
    let genre_ids : [Int]?
    let backdrop_path : String?
    let adult : Bool?
    let overview : String?
    let release_date : String?
}
