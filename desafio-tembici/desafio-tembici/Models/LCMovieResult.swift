//
//  LCMovieResult.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 15/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import Foundation

struct LCMovieResult : Codable {
    
    //
    //Success Proprierties
    let page : Int?
    let results : [LCMovie]?
    let total_results : Int?
    let total_pages : Int?
    
    //
    // Error Proprierties
    let status_message : String?
    let status_code : Int?
}
