//
//  LCHomeViewModel.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 15/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import UIKit

class LCHomeViewModel: NSObject {

    private var moviesArray : [LCMovie] = []
    private var currentPage = 0
    private var numberOfPages = 99
    private var errorMessage = "Failure to get movies"
    private var searchedText = ""
    
    private var hasPagination : Bool {
        get {
            return numberOfPages > currentPage
        }
    }
    
    var selectedCell = -1
    var selectedStatus = MovieRequestState.loading
    
    func numberOfItems(inSection section: Int) -> Int {
        return moviesArray.count
    }
    
    func movieTitle(forItem item:Int) -> String {
        guard let title = moviesArray[item].title else {
            assertionFailure("Movie without Title proprierty")
            return ""
        }
        
        return title
    }
    
    func movieImageURL(forItem item:Int) -> URL {
        guard let imagePath = moviesArray[item].poster_path else {
            assertionFailure("Movie without Image proprierty")
            return URL(dataRepresentation: Data(), relativeTo: nil)!
        }
        
        return URL(string: "\(ServerInfo.SeverUrl.imageURL)\(imagePath)")!
    }
    
    func releaseDate(forItem item:Int) -> String {
        guard let releaseDate = moviesArray[item].release_date else {
            assertionFailure("Movie without releaseDate proprierty")
            return ""
        }
        
        return releaseDate
    }
    
    func genreIds(forItem item:Int) -> [Int] {
        guard let genreIds = moviesArray[item].genre_ids else {
            assertionFailure("Movie without genreIds proprierty")
            return []
        }
        
        return genreIds
    }
    
    func overview(forItem item:Int) -> String {
        guard let overview = moviesArray[item].overview else {
            assertionFailure("Movie without overview proprierty")
            return ""
        }
        
        return overview
    }
    
    func isMovieFavorite(forItem item: Int) -> Bool {
        guard let isMovieFavorite = moviesArray[item].isFavorite else {
//            assertionFailure("Movie without isFavorite proprierty")
            return false
        }
        
        return isMovieFavorite
    }
    
    func movieID(forItem item: Int) -> Int {
        guard let movieID = moviesArray[item].id else {
            assertionFailure("Movie without id proprierty")
            return -1
        }
        
        return movieID
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        switch selectedStatus {
        case .error:
            return #imageLiteral(resourceName: "error")
        case .noSearchResult:
            return #imageLiteral(resourceName: "search_icon")
        case .sucess:
            return nil
        case .loading:
            return nil
        }
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> String? {
        switch selectedStatus {
        case .error:
            return errorMessage
        case .noSearchResult:
            return "Nenhum filme correspode à: \(searchedText)"
        case .sucess:
            return ""
        case .loading:
            return ""
        }
    }
    
    func customView(forEmptyDataSet scrollView: UIScrollView) -> UIView? {
        switch selectedStatus {
        case .error:
            return nil
        case .noSearchResult:
            return nil
        case .sucess:
            return nil
        case .loading:
            return UIActivityIndicatorView.init()
        }
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView) -> String? {
        switch selectedStatus {
        default:
            return "Try Again"
        }
    }
    
    func reload(completion: @escaping (_ success:Bool, _ movies: [LCMovie]?, _ errorMessage: Error?) -> Void) {
        resetInformations()
        
        listMovies(completion: completion)
    }
    
    private func resetInformations() {
        currentPage = 0
        numberOfPages = 99
        errorMessage = "Failure to get movies"
        searchedText = ""
        selectedStatus = .loading
        moviesArray = []
    }
    
    func listMovies(completion: @escaping (_ success:Bool, _ movies: [LCMovie]?, _ errorMessage: Error?) -> Void) {
        if hasPagination {
            currentPage += 1
            _ = APIClient.listMovies(currentPage) { (result) in
                self.selectedStatus = .loading
                
                if result.error == nil {
                    if let validResult = try? result.unwrap() {
                        guard let movies = validResult.results else {
                            self.selectedStatus = .error
                            self.errorMessage = "No movie return on success"
                            self.resetInformations()
                            
                            completion(false, nil, result.error)
                            return
                        }
                        
                        if movies.count > 0 {
                            self.selectedStatus = .sucess
                            self.moviesArray.append(contentsOf: movies)
                            self.numberOfPages = validResult.total_pages!
                            
                            completion(true, movies, nil)
                        } else {
                            self.selectedStatus = .noSearchResult
                            self.moviesArray.append(contentsOf: movies)
                            self.resetInformations()
                            
                            completion(false, nil, result.error)
                        }
                    } else {
                        self.errorMessage = "Failure to get movies"
                        self.selectedStatus = .error
                        self.resetInformations()
                        
                        completion(false, nil, result.error)
                    }
                } else {
                    guard let resultError = result.error else {
                        self.errorMessage = "Failure to get movies"
                        self.selectedStatus = .error
                        self.resetInformations()
                        
                        completion(false, nil, result.error)
                        return
                    }
                    
                    self.errorMessage = resultError.localizedDescription
                    self.selectedStatus = .error
                    self.resetInformations()
                    
                    completion(false, nil, result.error)
                }
            }
        }
    }
}
