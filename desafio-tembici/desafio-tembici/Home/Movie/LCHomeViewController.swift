//
//  LCHomeViewController.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 13/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import UIKit

class LCHomeViewController: UIViewController {

    //MARK: - Proprierties -
    @IBOutlet weak var searchBarOutlet: UISearchBar!
    @IBOutlet weak var movieCollectionViewOutlet: UICollectionView!
    
    let placeholderWidth : CGFloat = 54 // Replace with whatever value works for your placeholder text
    var offset = UIOffset()
    let viewModel = LCHomeViewModel()
    
    //MARK: - Life Cycle Methods -
    static func storyboardNavigationInit() -> UINavigationController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        guard let navigationVC = storyboard.instantiateInitialViewController() as? UINavigationController else {
            assertionFailure("There is no NavigationViewController as Initial View Controller")
            return UINavigationController()
        }
        
        return navigationVC
    }
    
    static func storyboardInit() -> LCHomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        guard let homeVC = storyboard.instantiateViewController(withIdentifier: String(describing: LCHomeViewController.self)) as? LCHomeViewController else {
            assertionFailure("There is no LCHomeViewController on storyboard")
            
            return LCHomeViewController()
        }
        
        return homeVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigation()
        configureSearchBar()
        configureCollectionView()
        
        viewModel.listMovies() { (success, movies, errorMessage) in
            if success {
                self.movieCollectionViewOutlet.reloadData()
            } else {
                self.movieCollectionViewOutlet.reloadEmptyDataSet()
            }
        }
    }
    
    //MARK: - Configurations Method -
    private func configureNavigation() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        title = "Movies"
    }
    
    private func configureSearchBar() {
        searchBarOutlet.barStyle = .default
        searchBarOutlet.isTranslucent = false
        searchBarOutlet.barTintColor = ColorConst.mainYellowColor
        searchBarOutlet.backgroundImage = UIImage()
        searchBarOutlet.layer.borderWidth = 1
        searchBarOutlet.layer.borderColor = UIColor.clear.cgColor
        
        offset = UIOffset(horizontal: (searchBarOutlet.frame.width - placeholderWidth) / 2, vertical: 0)
        searchBarOutlet.setPositionAdjustment(offset, for: .search)
    }
    
    private func configureCollectionView() {
        movieCollectionViewOutlet.emptyDataSetSource = self
        movieCollectionViewOutlet.emptyDataSetDelegate = self
    }
    
    //MARK: - Navigation Methods -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let movieDetail = segue.destination as? LCMovieDetailTableViewController else {
            assertionFailure("No LCMovieDetailTableViewController on storyboard")
            return
        }
        
        let selectedCell = viewModel.selectedCell
        let imageURL = viewModel.movieImageURL(forItem: selectedCell)
        let title = viewModel.movieTitle(forItem: selectedCell)
        let releaseDate = viewModel.releaseDate(forItem: selectedCell)
        let genreIds = viewModel.genreIds(forItem: selectedCell)
        let overview = viewModel.overview(forItem: selectedCell)
        
        movieDetail.configureView(imageURL, title: title, releaseDate: releaseDate, genreIds: genreIds, overview: overview)
    }
}

extension LCHomeViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectedCell = indexPath.item
        
        performSegue(withIdentifier: "toDetail", sender: nil)
    }
}

extension LCHomeViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems(inSection:section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: LCMovieCollectionViewCell.self), for: indexPath) as? LCMovieCollectionViewCell else {
            assertionFailure("No LCMovieCollectionViewCell registerd to CollectionView")
            return UICollectionViewCell()
        }
        
        let movieTitle = viewModel.movieTitle(forItem: indexPath.item)
        let movieImage = viewModel.movieImageURL(forItem: indexPath.item)
//        let isMovieFavorite = viewModel.isMovieFavorite(forItem: indexPath.item)
        
        cell.configureCell(movieTitle, movieImage: movieImage)
        
        if indexPath.row == viewModel.numberOfItems(inSection: indexPath.section) - 1 {
            viewModel.listMovies() { (success, movies, errorMessage) in
                if success {
                    self.movieCollectionViewOutlet.reloadData()
                    self.movieCollectionViewOutlet.reloadEmptyDataSet()
                } else {
                    self.movieCollectionViewOutlet.reloadData()
                    self.movieCollectionViewOutlet.reloadEmptyDataSet()
                }
            }
        }
        
        return cell
    }
}

//MARK: - EmptyDataSet
extension LCHomeViewController : EmptyDataSetSource {
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return viewModel.image(forEmptyDataSet:scrollView)
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        guard let text = viewModel.title(forEmptyDataSet:scrollView) else {
            return nil
        }
        let attributedString = NSAttributedString(string: text)
        
        return attributedString
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControlState) -> NSAttributedString? {
        guard let text = viewModel.buttonTitle(forEmptyDataSet:scrollView) else {
            return nil
        }
        let attributedString = NSAttributedString(string: text)
        
        return attributedString
    }
    
    func customView(forEmptyDataSet scrollView: UIScrollView) -> UIView? {
        return viewModel.customView(forEmptyDataSet:scrollView)
    }
    
}

extension LCHomeViewController : EmptyDataSetDelegate {
    
    func emptyDataSet(_ scrollView: UIScrollView, didTapButton button: UIButton) {
        viewModel.reload() { (success, movies, errorMessage) in
            if success {
                self.movieCollectionViewOutlet.reloadData()
                self.movieCollectionViewOutlet.reloadEmptyDataSet()
            } else {
                self.movieCollectionViewOutlet.reloadData()
                self.movieCollectionViewOutlet.reloadEmptyDataSet()
            }
        }
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTapView view: UIView) {
        viewModel.reload() { (success, movies, errorMessage) in
            if success {
                self.movieCollectionViewOutlet.reloadData()
                self.movieCollectionViewOutlet.reloadEmptyDataSet()
            } else {
                self.movieCollectionViewOutlet.reloadData()
                self.movieCollectionViewOutlet.reloadEmptyDataSet()
            }
        }
    }
}

//MARK: - UISearchBarDelegate -
extension LCHomeViewController : UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.alignPlaceholderLeft()
        
        return true
    }
    
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.alignPlaceholderCenter()
        
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}



