//
//  LCMovieDetailViewController.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 15/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import UIKit

class LCMovieDetailTableViewController: UITableViewController {

    @IBOutlet weak var movieImageViewOutlet: UIImageView!
    private var imageURL : URL?
    private var title : String?
    private var releaseDate : String?
    private var genereIds : [Int]
    private var 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigation()
    }
    
    func configureView(_ imageURL: URL, title: String, releaseDate: String, genreIds: [Int], overview:
        String) {
        
        if movieImageViewOutlet != nil {
            movieImageViewOutlet.image = nil //or keep any placeholder here
            let task = URLSession.shared.dataTask(with: imageURL) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async() {
                    self.movieImageViewOutlet.image = UIImage(data: data)
                }
            }
            task.resume()
        }

    }
    
    private func configureNavigation() {
        
       //create a new button
        let favoriteButton: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        favoriteButton.setImage(#imageLiteral(resourceName: "favorite_gray_icon"), for: .normal)
        favoriteButton.setImage(#imageLiteral(resourceName: "favorite_empty_icon"), for: .selected)
        //add function for button
        favoriteButton.addTarget(self, action: #selector(didPressFavoriteButton(_:)), for: .touchUpInside)
        //set frame
        favoriteButton.frame = CGRect(x: 0.0, y: 0.0, width: 53.0, height: 31.0)
        
        let barButton = UIBarButtonItem(customView: favoriteButton)
        //assign button to navigationbar
        navigationItem.rightBarButtonItem = barButton
    }
    
    @objc private func didPressFavoriteButton(_ sender: UIButton?) {
        
        sender?.isSelected = !(sender?.isSelected)!
        print("Lucas")
    }
}
