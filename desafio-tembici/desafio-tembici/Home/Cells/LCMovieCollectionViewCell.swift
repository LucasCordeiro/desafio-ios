//
//  LCMovieCollectionViewCell.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 15/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import UIKit

class LCMovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImageViewOutlet: UIImageView!
    @IBOutlet weak var movieTitleLabelOutlet: UILabel!
    @IBOutlet weak var favoriteButtonOutlet: UIButton!
    
    
    func configureCell(_ movieTitle: String, movieImage imageURL: URL) {
        
        movieImageViewOutlet.image = nil //or keep any placeholder here
        let task = URLSession.shared.dataTask(with: imageURL) { data, response, error in
            guard let data = data, error == nil else { return }
            
            DispatchQueue.main.async() {
                self.movieImageViewOutlet.image = UIImage(data: data)
            }
        }
        task.resume()
        
        movieTitleLabelOutlet.text = movieTitle
        backgroundColor = ColorConst.mainBlueColor
        movieTitleLabelOutlet.textColor = ColorConst.mainYellowColor
    }
    
    @IBAction func favouriteButtonPressed(_ sender: UIButton) {
        
    }
    
}
