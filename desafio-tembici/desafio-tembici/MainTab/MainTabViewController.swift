//
//  MainTabViewController.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 13/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import UIKit

/// TabBar that contains Home and Favorites
class MainTabViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBar.barTintColor = ColorConst.mainYellowColor
        tabBar.isTranslucent = false
        
        let homeVC = LCHomeViewController.storyboardNavigationInit()
        homeVC.navigationBar.barTintColor = ColorConst.mainYellowColor
        homeVC.tabBarItem.image = #imageLiteral(resourceName: "list_icon")
        
        let favoriteVC = LCFavoritesViewController.storyboardInit()
        favoriteVC.tabBarItem.image = #imageLiteral(resourceName: "favorite_empty_icon")
        
        setViewControllers([homeVC, favoriteVC], animated: true)
    }
}
