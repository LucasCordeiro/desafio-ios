//
//  LCFavoritesViewController.swift
//  desafio-tembici
//
//  Created by Lucas Cordeiro on 15/07/18.
//  Copyright © 2018 Lucas Cordeiro. All rights reserved.
//

import UIKit

class LCFavoritesViewController: UIViewController {

    static func storyboardInit() -> LCFavoritesViewController {
        let storyboard = UIStoryboard(name: "Favorites", bundle: nil)
        guard let favoriteVC = storyboard.instantiateViewController(withIdentifier: String(describing: LCFavoritesViewController.self)) as? LCFavoritesViewController else {
            assertionFailure("There is not LCFavoritesViewController on storyboard")
            return LCFavoritesViewController()
        }
        
        return favoriteVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
